# Examen de Projet de Système de Gestion de Base de Données - EAFC Namur
Ce projet a été réalisé sous certaines contraintes imposées par le chargé de cours. Chaque groupe, dont celui-ci, avait pour obligation d'utiliser une base de donnée commune sous MongoDB. Ce groupe-ci dont vous voyez le projet avait pour objectif de base d'affichage un catalogue en ligne des différents jeux de société de la base de donnée.
Vous constaterez que nous avons été plus loin que ce qui était demandé pour notre groupe "affichage mobile".

## Installation
Commencez par cloner le projet dans un dossier vide sur votre machine locale.
Vous devriez avoir deux sous-dossiers : 'client' et 'server'
Grâce à votre terminal, faites une installation npm dans chacun d'eux :
```
npm install
```
Une fois fait, il vous suffit de lancer le server avec npm start, ensuite le client avec la même commande.

Si vous avez configurer android studio ou xcode pour ouvrir un émulateur, vous pouvez suivre les instructions de votre client expo dans le terminal. Sinon, avec l'application mobile vous pouvez scanner le QR code ou entrer l'adresse de type "exp://" manuellement.

## Utilisation
Ce projet étant un projet scolaire, son utilisation ne peut être utilisée qu'à des fins pédagogiques et didactiques avec notre consentement. En effet, même si ce projet est en accès public, il n'en est pas moins soumis à des droits d'auteurs selon la législation de notre pays (Belgique). 
Toutefois, nous décidons de marquer notre accord systématique pour l'utilisation par des enseignants ou assimilés dans le cadre de leur profession de transmission de savoir au travers de ce fichier README.

## Credits
Ce projet a été réalisé par Marko Pasalic et Lyam Bernard dans le cadre de leur examen de troisième année en informatique de gestion au sein de l'EAFC Namur.
