import mongoose from 'mongoose';
const {Schema} = mongoose;
const inventorySchema = new Schema({
    uid: {
        type: Number,
    },
    gameID: {
        type: Number,
    },
    available: {
        type: Boolean,
    },
    locations: [{
        type: Object,
    }],
    resetCode: "",
},{ timestamps: true });

export default mongoose.model("Inventory", inventorySchema);