import mongoose from 'mongoose';
const {Schema} = mongoose;
const gameSchema = new Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    category: {
        type: String,
    },
    ageMin: {
        type: Number,
        required: true,
    },
    playersMin: {
        type: Number,
        required: true,
    },
    playersMax: {
        type: Number,
        required: true,
    },
    boxContent: [{
        what: {
            type: String,
            required: true,
        }
    }],
    votes: [{
        type: Object,
    }],
    locPrice: {
        type: Number,
        required: true,
    },
    caution: {
        type: Number,
        required: true,
    },
    resetCode: "",
},{ timestamps: true });

export default mongoose.model("Game", gameSchema);