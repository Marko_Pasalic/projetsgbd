import mongoose from 'mongoose';
const {Schema} = mongoose;
const userSchema = new Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
    },
    firstname: {
        type: String,
        trim: true,
        required: true,
    },
    lastname: {
        type: String,
        trim: true,
        required: true,
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true,
    },
    address: {
        street: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        zip: {
            type: String,
            required: true,
        },
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 64,
    },
    resetCode: "",
},{ timestamps: true });

export default mongoose.model("User", userSchema);