import Game from "../models/game";
import User from "../models/user";

require("dotenv").config();

// Find all games from DB
export const getAllGames = async (req, res) => {
    try {
        const result = await Game.find({});
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json(err);
    }
}

// find all names of games in DB
export const getGameName = async (req, res) => {
    try {
        const result = await Game.find({}, 'name id locPrice -_id');
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json(err);
    }
}

// Add a user comment
export const addComment = async (req, res) => {
    try {
        // Validate user entries
        const {comment} = req.body;
        if(!comment || comment === ''){
            return res.json({
                error: "Comment is required",
            });
        }else{
            // create user document
            const who = await User.findById(req.body.user.user._id, '_id id firstname lastname email address');
            // create game document
            const game = await Game.findById(req.body.item._id);
            // create votes document
            let vote = {who, 'rankVote': req.body.ranking, 'comment':req.body.comment}
            // push new comment in document
            game.votes.push(vote);
            game.save();
            return res.json(game);
        }
    } catch (err) {
        console.log(err);
    }
}