import User from "../models/user";
import {hashPassword, comparePassword} from "../helpers/auth";
import jwt from "jsonwebtoken";

require("dotenv").config();

// Creating new user
export const signup = async (req, res) => {
    console.log("Signup Hit");
    try {
        // validation
        const { firstname, lastname, email, street, streetnumber, city, zipcode, password } = req.body;
        if (!firstname) {
            return res.json({
                error: "Firstname is required",
            });
        }
        if (!lastname) {
            return res.json({
                error: "Lastname is required",
            });
        }
        if (!email) {
            return res.json({
                error: "Email is required",
            });
        }
        if (!street || !streetnumber || !city || !zipcode) {
            return res.json({
                error: "Complete address is required",
            });
        }
        if (!password || password.length < 6) {
            return res.json({
                error: "Password is required and should be 6 characters long",
            });
        }
        try{
            const exist = await User.findOne({ email });
            if (exist) {
                return res.json({
                    error: "Email is already taken",
                });
            }
        } catch (err) {
            res.status(500).json(err);
        }

        // hash password
        const hashedPassword = await hashPassword(password);
        try {
            const user = await new User({
                firstname,
                lastname,
                email,
                password: hashedPassword,
            } );

            user.address.street = street + ', ' + streetnumber;
            user.address.city = city;
            user.address.zip = zipcode;

            const lastID = (await User.find({}).sort({'createdAt':-1}).limit(1).select({'id':1, '_id':0}).lean().exec()).map(i => i.id).toString();
            
            user.id = parseInt(lastID)+1;
            user.save();
        // create signed token
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
            expiresIn: "7d",
        });
        const { password, ...rest } = user._doc;
            return res.json({
                token,
                user: rest,
        });
        } catch (err) {
            console.log(err);
        }
    } catch (err) {
        console.log(err);
    }
};

// Connection as user
export const signin = async (req, res) => {
    try {
        const { email, password } = req.body;
        // check if our db has user with that email
        const user = await User.findOne({ email });
        if (!user) {
            return res.json({
                error: "No user found",
            });
        }
        // check password
        const match = await comparePassword(password, user.password);
        if (!match) {
            return res.json({
                error: "Wrong password",
            });
        }
        // create signed token
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
            expiresIn: "7d",
        });
        user.password = undefined;
        user.secret = undefined;
        res.json({
            token,
            user,
        });
    } catch (err) {
        console.log(err);
        return res.status(400).send("Error. Try again.");
    }
};

// Updating address of user
export const updateAddress = async (req, res) => {
    try {
        // validation for all required fields
        const {street, streetnumber, city, zipcode} = req.body;
        if (!street || !streetnumber || !city || !zipcode) {
            return res.json({
                error: "Complete address is required",
            });
        } else {
            // find user by his mongodb id
            const user = await User.findById(req.body.user.user._id);
            // change datas
            user.address.street = street + ', ' + streetnumber;
            user.address.city = city;
            user.address.zip = zipcode;
            // update
            user.save();
            return res.json(user);
        }
    } catch (err) {
        console.log(err);
    }
}

// Update password of user
export const updatePassword = async (req, res) => {
    try {
        // check requirements for passwords
        const {password} = req.body;
        if(!password || password.length < 6) {
            return res.json({
                error: "Password is required and should be 6 characters long",
            });
        }else{
            // hash password
            const hashedPassword = await hashPassword(password);
            // find and update user password
            const user = await User.findByIdAndUpdate(req.body.user.user._id, {
                password: hashedPassword,
            });
            user.password = undefined;
            user.secret = undefined;
            return res.json(user);
        }
    } catch (err) {
        console.log(err);
    }
}