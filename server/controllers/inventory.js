import Inventory from "../models/inventory";
import Game from "../models/game";
import User from "../models/user";

require("dotenv").config();

// Get every inventories from a specific abstract game
export const getAllInventory = async (req, res) => {
    try {
        const id = parseInt(req.query.gameID);
        const result = await Inventory.find({gameID:id});
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json(err);
    }
}

// Add a new location to the DB
export const addLocation = async (req, res) => {
    try {
        const gameUID = req.body.gameUID;
        if (!gameUID || gameUID === ''){
            return res.json({
                error: "No unique id found",
            });
        }else{
            const physicalGame = await Inventory.findOne({uid: gameUID});
            const game = await Game.findOne({id: physicalGame.gameID});
            const who = await User.findById(req.body.user.user._id, '_id id firstname lastname email address');
            let location = {who, 'locPrice':game.locPrice, 'locFrom':new Date(), 'locTo':new Date(new Date().getTime() + 86400000), 'boxContentBefore':game.boxContent};
            physicalGame.locations.push(location);
            physicalGame.available = false;
            physicalGame.save();
            return res.json(physicalGame);
        }
    } catch (err) {
        console.log(err);
    }
}

// Get every locations of a specified user
export const getUserInventory = async (req, res) => {
    try {
        const id = parseInt(req.query.user.user.id);
        const inventories = await Inventory.find({'locations.who.id':id});
        res.status(200).json(inventories);
    } catch (err) {
        res.status(500).json(err);
    }
}