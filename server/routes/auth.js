import express from 'express';

const router = express.Router();

const {signup, signin, updateAddress, updatePassword} = require('../controllers/auth');

router.get('/', (req, res) => {
    return res.json({
        data: 'Auth API',
    });
});

router.post('/signup', signup);
router.post('/signin', signin);
router.post('/updateAddress', updateAddress);
router.post('/updatePassword', updatePassword);

export default router;