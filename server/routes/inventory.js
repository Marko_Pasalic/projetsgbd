import express from 'express';

const router = express.Router();

const {getAllInventory, addLocation, getUserInventory} = require('../controllers/inventory');

router.get('/', (req, res) => {
    return res.json({
        data: 'Game API',
    });
});

router.get('/getAllInventory', getAllInventory);
router.get('/getUserInventory', getUserInventory);
router.post('/addLocation', addLocation);

export default router;