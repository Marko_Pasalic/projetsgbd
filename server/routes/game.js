import express from 'express';

const router = express.Router();

const {getAllGames, addComment, getGameName} = require('../controllers/game');

router.get('/', (req, res) => {
    return res.json({
        data: 'Game API',
    });
});

router.get('/getAllGames', getAllGames);
router.get('/getGameName', getGameName);
router.post('/addComment', addComment);

export default router;