require('dotenv').config();
import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';

import authRoutes from './routes/auth';
import gameRoutes from './routes/game';
import inventoryRoutes from './routes/inventory';

const morgan = require('morgan');

const app = express();

mongoose
    .connect(process.env.DATABASE)
    .then(() => console.log('DB connected'))
    .catch((err) => console.log('DB CONNECTION ERROR: ', err));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use(morgan('dev'));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


// route middlewares
app.use('/api', authRoutes);
app.use('/api', gameRoutes);
app.use('/api', inventoryRoutes);

app.listen(8000, () => console.log('Server running on port 8000'));