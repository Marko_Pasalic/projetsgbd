import React, {useState, useContext} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import FooterList from '../components/footer/FooterList';
import {containers, texts} from '../components/style/Styles';
import Licorne from '../components/fancy/Licorne';
import AnimatedGradientText from '../components/fancy/AnimatedGradientText';
import Orval from '../components/fancy/Orval';
import Wave from '../components/fancy/Wave';
import Cube from '../components/fancy/Cube';

import ThemeContext from '../context/theme';


const Home = () => {
    const theme = useContext(ThemeContext);
    const [mode, setMode] = useState(false);

    return (

        <SafeAreaView style={[containers.spaceBetweenMainBox, { backgroundColor: theme.background }]}>
            <AnimatedGradientText style={texts.title}>Features Sandbox Display</AnimatedGradientText>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginBottom:15}}>
                    <Licorne />
                </View>
                <View style={{marginBottom:15}}>
                    <Orval />
                </View>
                <View>
                    <Wave />
                </View>
                {/*<View>
                    <Cube/>
                </View>*/}
            </ScrollView>
            <FooterList />
        </SafeAreaView>
    )
}

export default Home;