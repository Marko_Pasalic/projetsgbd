import {Text, View, TextInput, TouchableOpacity, Image} from 'react-native'
import React, {useState, useContext} from 'react'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthContext} from '../context/auth';
import {HOST_WITH_PORT} from '../constants/globals';

import {containers, texts, buttons, forms, MAIN_COLOR} from '../components/style/Styles'

const SignIn = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [state, setState] = useContext(AuthContext);

    const handleSubmit = async () => {
        try{
            if (email === '' || password === '') {
                alert('All fields are required');
                return;
            }else{
                const resp = await axios.post('http://' + HOST_WITH_PORT + '/api/signin', {email, password});
                if(resp.data.error){
                    console.log(resp.data.error);
                }else{
                    setState(resp.data);
                    await AsyncStorage.setItem('auth-rn', JSON.stringify(resp.data));
                    alert('Sign In Successful');
                    navigation.navigate('Home');
                }
            }
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <KeyboardAwareScrollView contentCotainerStyle={containers.mainBox}>
            <View style={{marginVertical:100}}>
            <View style={containers.logoContainer}>
                <Image source={require('../assets/logo.png')} style={containers.logoStyle} />
            </View>
                <Text style={texts.title}>Sign In</Text>
                <View style={{ marginHorizontal: 24 }}>
                    <Text style={texts.formText}>EMAIL</Text>
                    <TextInput style={forms.input} value={email} onChangeText={text => setEmail(text)} autoCompleteType='email' keyboardType='email-address' />
                </View>
                <View style={{ marginHorizontal: 24 }}>
                    <Text style={texts.formText}>PASSWORD</Text>
                    <TextInput style={forms.input} value={password} onChangeText={text => setPassword(text)} secureTextEntry={true} autoComplteType='password' />
                </View>
                <TouchableOpacity onPress={handleSubmit} style={buttons.buttonStyle}>
                    <Text style={buttons.buttonText}>Submit</Text>
                </TouchableOpacity>
                <Text style={{fontSize:12,textAlign:'center'}}>
                    Not yet registered? {' '}
                    <Text style={{color:MAIN_COLOR,fontWeight:'bold'}} onPress={() => navigation.navigate('SignUp')}>Sign Up</Text>
                </Text>
            </View>
        </KeyboardAwareScrollView>
    )
}

export default SignIn