import React, {useContext} from 'react';
import {Text, SafeAreaView, View, Image, ScrollView} from 'react-native';
import FooterList from '../components/footer/FooterList';
import {containers, texts} from '../components/style/Styles';
import themeContext from '../context/theme';
import AnimatedFireGradientText from '../components/fancy/AnimatedFireGradientText';

const About = () => {
    const theme = useContext(themeContext);


    return (
        <SafeAreaView style={[containers.spaceBetweenMainBox, {backgroundColor: theme.background}]}>
            <ScrollView>
                <Text style={[texts.title, {color: theme.color}]}>Equipe "Affichage mobile"</Text>
                <View>
                    <Text style={[texts.mainText, {color: theme.color}]}>Ludominique est un projet de gestion de base de donnée réalisé dans le cadre du cursus d'informatique de gestion donné par l'EAFC de Namur. Cette application est le résultat de plusieurs heures de travail par l'équipe responsable de la consultation mobile du catalogue de jeux de société. L'une des contraintes principales était l'utilisation d'une base de donnée commune à tous les groupes. Cette application est réalisée avec React-Native ce qui la rend totalement hybride, c'est-à-dire qui fonctionne autant sur Android que iOS. Retrouvez ci-dessous une petite fiche des membres de cette équipe "affichage mobile" à la synergie inégalable.</Text>
                </View>
                <View style={{marginTop:24}}>
                    <View style={containers.logoContainer}>
                        <Image source={require('../assets/marko.jpg')} style={{width:150, height: 200, borderRadius:15}}/>
                    </View>
                    <AnimatedFireGradientText style={texts.title}>Marko Pasalic</AnimatedFireGradientText>
                    <Text style={[texts.mainText, {color: theme.color}]}>Marko est un fervent défenseur du "C'est sûr, avec ça on va avoir 100% !". Il est indéniable que ses talents ont permis de faire avancer le projet à grand pas. Sans sa persévérance, l'équipe "affichage mobile" aurait probablement abandonné l'utilisation de react-native et donc d'avoir une application totalement hybride. La majorité des idées permettant de débloquer des situations complexes viennent de lui.</Text>
                </View>
                <View style={{marginTop:24}}>
                    <View style={containers.logoContainer}>
                        <Image source={require('../assets/lyam.jpg')} style={{width:150, height: 200, borderRadius:15}}/>
                    </View>
                    <AnimatedFireGradientText style={texts.title}>Lyam Bernard</AnimatedFireGradientText>
                    <Text style={[texts.mainText, {color: theme.color}]}>Lyam est probablement celui qui a débloqué la situation lorsqu'elle était critique en proposant un massacre de Skavens sur Vermintide 2. Grâce à la motivation-attitude de Marko, il s'est démené pour réussir à rendre l'application 100% hybride et à assurer une connexion stable avec la base de donnée. C'est lui qui met en application les idées de Marko pour débloquer une situation compliquée.</Text>
                </View>
                <View style={{marginTop:24}}>
                    <Text style={[texts.title, {color: theme.color}]}>A la mémoire de Christophe, petit ange parti trop tôt</Text>
                    <View style={containers.logoContainer}>
                        <Image source={require('../assets/tof.jpg')} style={{width:150, height: 200, borderRadius:15}}/>
                    </View>
                </View>
            </ScrollView>
            <FooterList />
        </SafeAreaView>
    )
}

export default About