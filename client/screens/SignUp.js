import React, {useState, useContext} from 'react';
import {Text, View, ScrollView, TextInput, TouchableOpacity, Image} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthContext} from '../context/auth';
import {HOST_WITH_PORT} from '../constants/globals';

import {containers, texts, buttons, forms, MAIN_COLOR} from '../components/style/Styles';

const SignUp = ({navigation}) => {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [street, setStreet] = useState("");
    const [streetnumber, setStreetnumber] = useState("");
    const [city, setCity] = useState("");
    const [zipcode, setZipcode] = useState("");
    const [state, setState] = useContext(AuthContext);

    const handleSubmit = async () => {
        try {
            if (firstname === '' || lastname === '' || email === '' || street === '' || streetnumber === '' || city === '' || zipcode === '' || password === ''){
                alert('All fields are required');
                return;
            }else{
                const resp = await axios.post('http://' + HOST_WITH_PORT + '/api/signup', {firstname, lastname, email, street, streetnumber, city, zipcode, password});
                if(resp.data.error){
                    console.log(resp.data.error);
                }else{
                    setState(resp.data);
                    await AsyncStorage.setItem('auth-rn', JSON.stringify(resp.data));
                    alert('Sign Up Successful');
                    navigation.navigate('Home');
                }
            }
        } catch (err) {
            console.log(err);
        }
    }
   
    return (
        <KeyboardAwareScrollView contentContainerStyle={containers.container}>
            <ScrollView style={{marginVertical:100}}>
                <View style={containers.logoContainer}>
                    <Image source={require('../assets/logo.png')} style={containers.logoStyle} />
                </View>
                <Text style={texts.title}>SignUp</Text>
                <View style={{marginHorizontal:24}}>
                    <Text style={texts.formText}>FIRSTNAME</Text>
                    <TextInput style={forms.input} value={firstname} onChangeText={(text => setFirstname(text))} autoCapitalize='words' autoCorrect={false} />
                </View>
                <View style={{marginHorizontal:24}}>
                    <Text style={texts.formText}>LASTNAME</Text>
                    <TextInput style={forms.input} value={lastname} onChangeText={(text => setLastname(text))} autoCapitalize='words' autoCorrect={false} />
                </View>
                <View style={{marginHorizontal:24}}>
                    <Text style={texts.formText}>EMAIL</Text>
                    <TextInput style={forms.input} value={email} onChangeText={(text => setEmail(text))} autoCompleteType='email' keyboardType='email-address' />
                </View>
                <View style={{marginHorizontal:24}}>
                    <Text style={texts.formText}>STREET</Text>
                    <TextInput style={forms.input} value={street} onChangeText={(text => setStreet(text))} autoCompleteType='street-address'/>
                    <Text style={texts.formText}>STREET N°</Text>
                    <TextInput style={forms.input} value={streetnumber} onChangeText={(text => setStreetnumber(text))}/>
                    <Text style={texts.formText}>CITY</Text>
                    <TextInput style={forms.input} value={city} onChangeText={(text => setCity(text))}/>
                    <Text style={texts.formText}>ZIPCODE</Text>
                    <TextInput style={forms.input} value={zipcode} onChangeText={(text => setZipcode(text))} autoCompleteType='postal-code' keyboardType='number-pad'/>
                </View>
                <View style={{marginHorizontal:24}}>
                    <Text style={texts.formText}>PASSWORD</Text>
                    <TextInput style={forms.input} value={password} onChangeText={(text => setPassword(text))} secureTextEntry={true} autoCompleteType='password' />
                </View>
                <TouchableOpacity onPress={handleSubmit} style={buttons.buttonStyle}>
                    <Text style={buttons.buttonText}>Submit</Text>
                </TouchableOpacity>
                <Text style={{fontSize:12,textAlign:'center'}}>
                    Already Joined ? {' '}
                    <Text style={{color:MAIN_COLOR,fontWeight:'bold'}} onPress={() => navigation.navigate('SignIn')}>Sign In</Text>
                </Text>
            </ScrollView>
        </KeyboardAwareScrollView>
    )
}

export default SignUp