import React, {useContext, useEffect, useState} from 'react';
import {Text, View, SafeAreaView, TouchableOpacity} from 'react-native';
import FooterList from '../components/footer/FooterList';
import {containers, buttons} from '../components/style/Styles';
import axios from 'axios';
import {HOST_WITH_PORT} from '../constants/globals';
import {GamesContext} from '../context/game';
import AsyncStorage from '@react-native-async-storage/async-storage';


import themeContext from '../context/theme';


const GameReservation = ({ route, navigation }) => {
    const { item } = route.params;
    const [game, setGame] = useState([]);
    const [state, setState] = useContext(GamesContext);
    const theme = useContext(themeContext);

    let contentID= 0;

    useEffect(() => {
        const fetchInventories = async () => {
            try {
                const gameID = item.id;
                const response = await axios.get("http://" + HOST_WITH_PORT + "/api/getAllInventory", {params:{gameID}});
                setGame(response.data);
            } catch (err) {
                console.error(err);
            }
        }
        fetchInventories();

    }, []);

    const handleReserve = async (gameUID) => {
        try {
            let storedData = await AsyncStorage.getItem('auth-rn');
            const user = JSON.parse(storedData);
            const resp = await axios.post("http://" + HOST_WITH_PORT + "/api/addLocation", {gameUID, user});
            const data = resp.data;
            if(data.error){
                alert(data.error);
            }else{
                alert("Réservation enregistrée");
                navigation.navigate('Home');
            }
        } catch (err) {
            console.log(err);
        }
    }

    const getCurrentDate = () => {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
    
        //Alert.alert(date + '-' + month + '-' + year);
        // You can turn it in to your desired format
        return date + '-' + month + '-' + year;//format: d-m-y;
    }
    const getTomorrowDate = () => {
        var date = new Date().getDate() + 1;
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
    
        //Alert.alert(date + '-' + month + '-' + year);
        // You can turn it in to your desired format
        return date + '-' + month + '-' + year;//format: d-m-y;
    }

    return (
        
        <SafeAreaView style={[containers.spaceBetweenMainBox, {backgroundColor: theme.background}]}>
            <View>
                {game.map(obj => (
                    <View key={++contentID} style={{borderBottomWidth: 2, borderRadius: 10,borderBottomColor: theme.color, marginBottom: 10}}>
                        <Text style={{color: theme.color}}>{item.name} - {obj.uid}</Text>
                        {obj.available == true ?
                            <View>
                                <Text style={{color: theme.color}}>Disponibilité : Oui</Text>
                                <TouchableOpacity style={[buttons.buttonStyle, {marginTop: 5}]} onPress={() => handleReserve(obj.uid)}>
                                    <Text style={buttons.buttonText}>Réserver du {getCurrentDate()} au {getTomorrowDate()}</Text>
                                </TouchableOpacity>
                            </View>
                        :
                        <View>
                            <Text style={{color: theme.color}}>Disponibilité : Non</Text>
                            <TouchableOpacity style={[buttons.disabledButtonStyle, {marginTop: 5}]} disabled={true}>
                                <Text style={buttons.buttonText}>Non disponible du {getCurrentDate()} au {getTomorrowDate()}</Text>
                            </TouchableOpacity>
                        </View>
                        }
                    </View>
                ))}
            </View>
            <FooterList />
        </SafeAreaView>
    )
}

export default GameReservation;