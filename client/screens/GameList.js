import React, {useEffect, useState, useContext} from 'react';
import {Text, SafeAreaView, ScrollView, View, TouchableOpacity, Image} from 'react-native';
import {GamesContext} from '../context/game';
import axios from 'axios';
import {HOST_WITH_PORT} from '../constants/globals';
import {SearchBar} from '@rneui/themed';

import FooterList from '../components/footer/FooterList';
import {containers, texts} from '../components/style/Styles';
import themeContext from '../context/theme';

const GameList = ({ navigation }) => {

    const [game, setGame] = useState([]);
    const [state, setState] = useContext(GamesContext);
    const [search, setSearch] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [masterDataSource, setMasterDataSource] = useState([]);
    const theme = useContext(themeContext);

    useEffect(() => {
        const fetchGames = async () => {
            try {
                const response = await axios.get("http://" + HOST_WITH_PORT + "/api/getAllGames");
                setGame(response.data);
                setFilteredData(response.data);
                setMasterDataSource(response.data);
            } catch (err) {
                console.error(err);
            }
        }
        fetchGames();

    }, []);

    const searchFilterFunction = (text) => {
        if (text) {
            const newData = masterDataSource.filter(function (item) {
                if (item.name.toLowerCase().includes(text.toLowerCase())) {
                    const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                }
                if (item.category.toLowerCase().includes(text.toLowerCase())) {
                    const itemData = item.category ? item.category.toUpperCase() : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                }
            })
            setFilteredData(newData);
            setSearch(text);
        } else {
            setFilteredData(masterDataSource);
            setSearch(text);

        }
    }

    const handlePress = async (item) => {
        try {
            navigation.navigate("GameDetails", { item: item });
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <SafeAreaView style={[containers.spaceBetweenMainBox, {backgroundColor: theme.background}]}>
            <Text style={[texts.title, {color: theme.color}]}>Game List</Text>
            <ScrollView showVerticalScrollIndicator={false}>
                <SearchBar
                    placeholder="Search here"
                    onChangeText={(text) => searchFilterFunction(text)}
                    onClearText={(text) => searchFilterFunction('')}
                    value={search.toString()}
                />
                {filteredData && filteredData.map(item => (
                    <View key={item._id} style={{ alignItems: 'center', marginTop: 8 }}>
                        <View style={[containers.box, {backgroundColor: theme.background}]} >
                            <TouchableOpacity key={item._id} onPress={() => handlePress(item)}
                                accessible={true}
                                accessibilityLabel='You can click on me'
                                accessibilityHint='Navigates to the Game Details screen'
                                accessibilityRole={'button'}>
                                <Image style={containers.boxImage} source={{ uri: 'https://source.unsplash.com/random/?Boardgame' }} />
                                <View style={{ padding: 5, height: 50 }}>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 18, marginTop: 10, marginBottom: 8, color: theme.color }}>{item.name}</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: theme.color }}>Catégorie : </Text>
                                            <Text style= {{color: theme.color}}>{item.category}</Text>
                                        </Text>
                                        <Text style={{ textAlign: 'right', fontWeight: 'bold', fontSize: 20, color: '#f36e31', marginRight: 10}}>{item.locPrice}€/jour</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                ))}
            </ScrollView>
            <FooterList />
        </SafeAreaView>
    )
}

export default GameList;