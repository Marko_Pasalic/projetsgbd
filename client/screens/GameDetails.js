import React, {useState, useContext} from 'react';
import {TextInput, Text, SafeAreaView, ScrollView, View, TouchableOpacity, Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {GamesContext} from '../context/game';
import themeContext from '../context/theme';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {HOST_WITH_PORT} from '../constants/globals';
import InputSpinner from "react-native-input-spinner";

import Stars from 'react-native-stars';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import FooterList from '../components/footer/FooterList';
import {containers, texts, buttons, forms} from '../components/style/Styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const GameDetails = ({ route, navigation }) => {

    const { item } = route.params;
    const theme = useContext(themeContext);
    const maxLength = 150;
    const [textLength, setTextLength] = useState(maxLength);
    const commentInput = React.createRef();
    const [comment, setComment] = useState('');
    const [state, setState] = useContext(GamesContext);
    const [ranking, setRanking] = useState(0);

    let voteID= 0;

    const addComment = async () => {
        try {
            let storedData = await AsyncStorage.getItem('auth-rn');
            const user = JSON.parse(storedData);
            const resp = await axios.post("http://" + HOST_WITH_PORT + "/api/addComment", {comment, user, item, ranking});
            const data = resp.data;
            
            if(data.error){
                alert(data.error);
            }else{
                commentInput.current.clear();
                setTextLength(maxLength);
                setComment('');
                navigation.navigate('Home');
            }
        } catch (error){
            alert('Comment publish failed');
            console.log(error);
        }
    }

    const handlePress = async (item) => {
        try {
            navigation.navigate("GameReservation", { item: item });
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <SafeAreaView style={[containers.spaceBetweenMainBox, {backgroundColor: theme.background}]}>
            <Text style={[texts.title, {color: theme.color}]} accessibilityHint='This is the title of the game'>{item.name} Details</Text>
            <KeyboardAwareScrollView>
            <ScrollView showVerticalScroolIndicator={false} style={{padding:3}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <View>
                        <Image style={{width:180, height:200,borderWidth:1}} source={{ uri: 'https://source.unsplash.com/random/?Boardgame' }} />
                    </View>
                    <View style={{marginRight:15}}>
                        <View style={{flexDirection: 'row',marginBottom:5}}>
                            <FontAwesome5 name="user-plus" size={16} style={{color: theme.color}} ></FontAwesome5>
                            <Text style={{color: theme.color, fontSize:16}}>  à partir de {item.ageMin} ans</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesome5 name="users" size={16} style={{color: theme.color}} ></FontAwesome5>
                            <Text style={{color: theme.color, fontSize:16}}>  {item.playersMin} à {item.playersMax} joueur(s)</Text>
                        </View>
                        <View>
                            <Text style={{ color: theme.color, fontWeight: 'bold', fontSize: 15, marginTop: 5 }}>Location :</Text>
                            <Text style={{ color: 'orange', fontWeight: 'bold', fontSize: 20, marginTop: 5 }} accessibilityHint='Prix de la location'>{item.locPrice}€ / jour</Text>
                            <Text style={{ color: theme.color, fontWeight: 'bold', fontSize: 15, marginTop: 5 }}>Caution :</Text>
                            <Text style={{ color: 'orange', fontWeight: 'bold', fontSize: 20, marginTop: 5 }} accessibilityHint='Prix de la caution'>{item.caution}€</Text>
                        </View>
                        <View>
                            <TouchableOpacity style={[buttons.buttonStyle, {marginTop:5}]} onPress={() => handlePress(item)}>
                                <Text style={buttons.buttonText}>Réservations</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View>
                        <Text style={{ color: theme.color, fontWeight: 'bold', fontSize: 18, paddingBottom: 3}}>Description :</Text>
                        <Text style={{ color: theme.color }} accesibilityHint='Description du jeu'>{item.description}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ marginTop: 10 , marginBottom: 10}}>
                                <Text style={{ color: theme.color, fontWeight: 'bold', fontSize: 18 }}>Catégorie : </Text>
                                <Text style={{ color: theme.color }}>{item.category}</Text>
                            </Text>
                        </View>
                    </View>
                    <View>
                        <View style={{ borderBottomWidth: 2, borderRadius: 10, borderColor: theme.color }}>
                            <Text accessible={true} accessibilityLabel='There are users comments' style={{ fontWeight: 'bold', fontSize: 18, marginBottom: 5, color: theme.color,textAlign:'center' }}>Commentaire :</Text>
                        </View>
                        <View>
                            {item.votes.map(content => {
                                return (
                                    <View key={++voteID} style={{ borderBottomWidth: 2, borderRadius: 10, marginTop: 10, borderColor: theme.color }} accessible={true}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                            <Text style={{ color: theme.color }}>{content.who.firstname} {content.who.lastname}</Text>
                                            <Stars
                                                half={true}
                                                display={content.rankVote / 2}
                                                spacing={8}
                                                count={5}
                                                starSize={30}
                                                fullStar={<FontAwesome name={'star'} style={{ color: 'orange' }} />}
                                                emptyStar={<FontAwesome name={'star-o'} style={{ color: 'orange' }} />}
                                                halfStar={<FontAwesome name={'star-half-empty'} style={{ color: 'orange' }} />}
                                            />
                                        </View>
                                        <Text style={{ marginBottom: 2, color: theme.color, marginLeft:5 }}>{content.comment}</Text>
                                    </View>
                                )
                            })}
                            <View>
                                <TextInput ref={commentInput} style={forms.commentInput} multiline={true} maxLength={maxLength} numberOfLines={3} placeholder={'Écrire un commentaire'} placeholderTextColor={theme.color} onChangeText={(text) => { setTextLength(maxLength - text.length); setComment(text); }} />
                                <Text style={[forms.inputLength, { color: theme.color }]}>{textLength}/{maxLength} Characters</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                                    <Text style={[texts.title, { color: theme.color }]}>Ranking : </Text>
                                    <InputSpinner
                                        value={0}
                                        onChange={(value) => setRanking(value)}
                                        style={[
                                            { minWidth: 150, flex: 1, marginRight: 10, minWidth: 150 } /*reset minWidth set on Style.spinner*/,
                                        ]}
                                        skin="modern"
                                        min={0}
                                        max={10}
                                        step={1}
                                        color='orange'
                                    />
                                    <Text style={[texts.title, { color: theme.color }]}>/ 10</Text>
                                </View>

                                <TouchableOpacity onPress={addComment} style={[buttons.buttonStyle, {marginHorizontal: 15}]}>
                                    <Text style={buttons.buttonText}>Publier</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAwareScrollView>
            <FooterList />
        </SafeAreaView>
    )
}

export default GameDetails;