import React, {useContext, useEffect, useState} from 'react';
import {View, Text, SafeAreaView, ScrollView, TouchableOpacity, TextInput} from 'react-native';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import FooterList from '../components/footer/FooterList';
import {containers, texts, buttons, forms} from '../components/style/Styles';
import {AuthContext} from '../context/auth';
import themeContext from '../context/theme';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {HOST_WITH_PORT} from '../constants/globals';
import GlowingLogo from '../components/fancy/GlowingLogo';
import Moment from 'moment';

const Account = ({navigation}) => {
    const [email, setEmail] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [password, setPassword] = useState('');
    const [address, setAddress] = useState('');
    const [addressStreet, setAddressStreet] = useState('');
    const [addressCity, setAddressCity] = useState('');
    const [addressZip, setAddressZip] = useState('');

    const [street, setStreet] = useState('');
    const [streetnumber, setStreetnumber] = useState('');
    const [city, setCity] = useState('');
    const [zipcode, setZipcode] = useState('');
    const [state, setState] = useContext(AuthContext);

    const [inventories, setInventories] = useState([]);
    const [gamesNames, setGamesNames] = useState([]);
    let viewID = 0;

    const theme = useContext(themeContext);


    useEffect(() => {
        const fetchInventories = async () => {
            try {
                let storedData = await AsyncStorage.getItem('auth-rn');
                const user = JSON.parse(storedData);
                const response = await axios.get("http://" + HOST_WITH_PORT + "/api/getUserInventory", {params:{user}});
                setInventories(response.data);
            } catch (err) {
                console.error(err);
            }
        }
        fetchInventories();

        const fetchGameNames = async () => {
            const response = await axios.get("http://" + HOST_WITH_PORT + "/api/getGameName");
            setGamesNames(response.data);
        }
        fetchGameNames();

        if (state) {
            const {firstname, lastname, email, address} = state.user;
            setFirstname(firstname);
            setLastname(lastname);
            setEmail(email);
            setAddress(address);
            setAddressStreet(address.street);
            setAddressCity(address.city);
            setAddressZip(address.zip);
        }
    }, [state]);

    const handleAddressUpdate = async () => {
        try {
            let storedData = await AsyncStorage.getItem('auth-rn');
            const user = JSON.parse(storedData);
            const resp = await axios.post("http://" + HOST_WITH_PORT + "/api/updateAddress", {street, streetnumber, city, zipcode, user});
            const data = resp.data;
            if(data.error){
                alert(data.error);
            }else{
                alert('Address updated succesfully');
                setAddressStreet(street + ', ' + streetnumber);
                setAddressCity(city);
                setAddressZip(zipcode);
                setStreet('');
                setStreetnumber('');
                setCity('');
                setZipcode('');
            }
        } catch (error){
            alert('Address update failed');
            console.log(error);
        }
    }

    const handlePasswordUpdate = async () => {
        try {
            let storedData = await AsyncStorage.getItem('auth-rn');
            const user = JSON.parse(storedData);
            const resp = await axios.post("http://" + HOST_WITH_PORT + "/api/updatePassword", {password, user});
            const data = resp.data;
            if(data.error){
                alert(data.error);
            }else{
                alert('Password updated successfully');
                setPassword('');
            }
        } catch (error) {
            alert('Password update failed');
            console.log(error);
        }
    }

    return (
        <SafeAreaView style={[containers.spaceBetweenMainBox, {backgroundColor: theme.background}]}>
            <KeyboardAwareScrollView>
            <ScrollView>
                <View style={containers.logoContainer}>
                    <GlowingLogo />
                </View>
                <View style={{marginTop:35}}>
                    <Text style={[texts.title, {color: theme.color}]}>{firstname} {lastname}</Text>
                    <View style={{marginTop:5}}>
                        <Text style={[texts.accountText, {color: theme.color}]}>{email}</Text>
                    </View>
                    <View style={{marginTop: 24}}>
                        <Text style={{textAlign:'center',fontSize:20,textDecorationLine:'underline', color: theme.color}}>Adresse</Text>
                        <Text style={[texts.accountText, {color: theme.color}]}>{addressStreet}</Text>
                        <Text style={[texts.accountText, {color: theme.color}]}>{addressCity} - {addressZip}</Text>
                    </View>
                    <Collapse style={{marginTop: 24}}>
                        <CollapseHeader style={{justifyContent:'center', flexDirection:'row'}}>
                            <Text style={{textAlign:'center',fontSize:20,textDecorationLine:'underline', color: theme.color}}>Modification d'adresse</Text>
                            <FontAwesome5 name='bars' size={20} style={{marginTop:4,marginLeft:10, color: theme.color}}/>
                        </CollapseHeader>
                        <CollapseBody style={{margin:15}}>
                            <Text style={[texts.formText, {color: theme.color}]}>Street</Text>
                            <TextInput style={[forms.input, {color: theme.color}]} value={street} onChangeText={(text => setStreet(text))} autoCompleteType='street-address'/>
                            <Text style={[texts.formText, {color: theme.color}]}>Street number</Text>
                            <TextInput style={[forms.input, {color: theme.color}]} value={streetnumber} onChangeText={(text => setStreetnumber(text))}/>
                            <Text style={[texts.formText, {color: theme.color}]}>City</Text>
                            <TextInput style={[forms.input, {color: theme.color}]} value={city} onChangeText={(text => setCity(text))}/>
                            <Text style={[texts.formText, {color: theme.color}]}>Zip</Text>
                            <TextInput style={[forms.input, {color: theme.color}]} value={zipcode} onChangeText={(text => setZipcode(text))} autoCompleteType='postal-code' keyboardType='number-pad'/>
                            <TouchableOpacity onPress={handleAddressUpdate} style={buttons.buttonStyle}>
                                <Text style={buttons.buttonText}>Update</Text>
                            </TouchableOpacity>
                        </CollapseBody>
                    </Collapse>
                    <Collapse style={{marginTop: 24}}>
                        <CollapseHeader style={{justifyContent:'center', flexDirection:'row'}}>
                            <Text style={{textAlign:'center',fontSize:20,textDecorationLine:'underline', color: theme.color}}>Modification de mot de passe</Text>
                            <FontAwesome5 name='bars' size={20} style={{marginTop:4,marginLeft:10, color: theme.color}}/>
                        </CollapseHeader>
                        <CollapseBody style={{margin:15}}>
                            <Text style={[texts.formText, {color: theme.color}]}>New Password</Text>
                            <TextInput style={[forms.input, {color: theme.color}]} value={password} onChangeText={(text => setPassword(text))} secureTextEntry={true} autoCompleteType='password'/>
                            <TouchableOpacity onPress={handlePasswordUpdate} style={buttons.buttonStyle}>
                                <Text style={[buttons.buttonText, {color: theme.color}]}>Update</Text>
                            </TouchableOpacity>
                        </CollapseBody>
                    </Collapse>
                </View>
                <Collapse style={{marginTop:50}}>
                    <CollapseHeader style={{justifyContent:'center', flexDirection:'row'}}>
                        <Text style={[texts.title, {color: theme.color}]}>Historique de location</Text>
                        <FontAwesome5 name='bars' size={25} style={{marginTop:8,marginLeft:10, color: theme.color}}/>
                    </CollapseHeader>
                    <CollapseBody>
                        {inventories.map(inventory => (
                            <View key={++viewID} style={{flexDirection: 'row'}}>
                                <Text style={{margin:5, color: theme.color}}>
                                    {viewID+1}{') '}
                                    {gamesNames.find(obj => {
                                        return obj.id === inventory.gameID;
                                    }).name}{' '}
                                    à {inventory.locations.map(obj => (obj.locPrice))}€/jour{' '}
                                    du {inventory.locations.map(obj => (Moment(obj.locFrom).format('D MMM Y')))}{' '}
                                    au {inventory.locations.map(obj => (Moment(obj.locTo).format('D MMM Y')))}</Text>
                            </View>
                        ))}
                    </CollapseBody>
                </Collapse>
            </ScrollView>
            </KeyboardAwareScrollView>
            <FooterList />
        </SafeAreaView>
    )
}

export default Account