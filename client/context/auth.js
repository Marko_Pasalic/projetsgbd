import React, {useState, useEffect, createContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AuthContext = createContext();

const AuthProvider = ({children}) => {
    const [state, setState] = useState({
        user: undefined,
        token: "",
    });

    useEffect(() => {
        const loadFromAsyncStorage = async () => {
            try{
                let data = await AsyncStorage.getItem('auth-rn');
                const parsed = JSON.parse(data);
                setState({...state, user: parsed.user, token: parsed.token});
            } catch (err) {
                //console.log(err);
            }
        };
        loadFromAsyncStorage();
    }, []);

    return (
        <AuthContext.Provider value={[state, setState]}>
            {children}
        </AuthContext.Provider>
    );
};

export {AuthContext, AuthProvider};