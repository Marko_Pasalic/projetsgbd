import React, {useState, useEffect, createContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const GamesContext = createContext();

const GamesProvider = ({children}) => {
    const [state, setState] = useState({
        game: undefined,
    });

    useEffect(() => {
        const loadFromAsyncStorage = async () => {
            try{
                let data = await AsyncStorage.getItem('game-rn');
                const parsed = JSON.parse(data);
                setState({...state, game: parsed.game});
            } catch (err) {
                //console.log(err);
            }
        };
        loadFromAsyncStorage();
    }, []);

    return (
        <GamesContext.Provider value={[state, setState]}>
            {children}
        </GamesContext.Provider>
    );
};

export {GamesContext, GamesProvider};