import Constants from 'expo-constants';

// Used to find the server ip where expo is running
const {manifest} = Constants;

const HOST=manifest.debuggerHost.split(':').shift();

const PORT='8000'

const HOST_WITH_PORT=HOST+':'+PORT

export {HOST, PORT, HOST_WITH_PORT}