import React, {useContext, useState} from 'react';
import {SafeAreaView, Switch} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';

import themeContext from '../../context/theme';

import FontAwesome5 from 'react-native-vector-icons/MaterialCommunityIcons';

const HeaderTabsLeft = () => {

    const theme = useContext(themeContext);
    const [mode, setMode] = useState(false);

    return (
        <SafeAreaView style={{flexDirection: 'row'}}>
            <FontAwesome5 name='theme-light-dark' size={30} style={{color: 'black'}} />
            <Switch style={{marginLeft: 5}}value={mode} onValueChange={() => {
                setMode((value) => !value);
                EventRegister.emit("changeTheme", mode);
            }} />
        </SafeAreaView>
    );
}

export default HeaderTabsLeft