import React, {useContext} from 'react';
import {TouchableOpacity, SafeAreaView} from 'react-native';

import {AuthContext } from '../../context/auth';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HeaderTabs = () => {
    const [state, setState] = useContext(AuthContext);

    const signOut = async () => {
        setState({ token: '', user: undefined });
        await AsyncStorage.removeItem('auth-rn');
    };

    return (
        <SafeAreaView>
            <TouchableOpacity onPress={signOut}>
                <FontAwesome5 name='sign-out-alt' size={25} color='black' />
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export default HeaderTabs