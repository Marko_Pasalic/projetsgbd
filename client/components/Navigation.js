import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import NavigationScreen from './NavigationScreen';
import {AuthProvider} from '../context/auth';
import {EventRegister} from 'react-native-event-listeners';
import ThemeContext from '../context/theme';
import {theme} from './style/Styles';
import {StatusBar} from 'expo-status-bar';


const Navigation = () => {
    const [mode, setMode] = useState(true);

    useEffect(() => {
        let eventListener = EventRegister.addEventListener("changeTheme", (data) => {
            setMode(data);
        });
        return () => {
            EventRegister.removeEventListener(eventListener);
        };
    });

    return (
        <ThemeContext.Provider value={mode === true ? theme.light : theme.dark}>
            <NavigationContainer>
                <AuthProvider>
                    <StatusBar backgroundColor={"#fff"}/>
                    <NavigationScreen />
                </AuthProvider>
            </NavigationContainer>
        </ThemeContext.Provider>
    )
}

export default Navigation