import React, {useContext} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {AuthContext} from '../context/auth';
import {GamesProvider} from '../context/game';

import SignUp from '../screens/SignUp';
import SignIn from '../screens/SignIn';
import Home from '../screens/Home';
import Account from '../screens/Account';
import About from '../screens/About';
import GameList from '../screens/GameList';
import GameDetails from '../screens/GameDetails';
import GameReservation from '../screens/GameReservation';
import HeaderTabs from './header/HeaderTabs';
import HeaderTabsLeft from './header/HeaderTabsLeft';


const Stack = createNativeStackNavigator();


const NavigationScreen = () => {
    const [state, setState] = useContext(AuthContext);
    const authenticated = state && state.token !== "" && state.user !== null;



    return (
        <GamesProvider>
            <Stack.Navigator initialRouteName='Account'>
                {authenticated ? (
                    <>
                        <Stack.Screen name='Home' component={Home} options={{ headerLeft: () => <HeaderTabsLeft/>, headerRight: () => <HeaderTabs />,orientation: 'all'}} />
                        <Stack.Screen name='Account' component={Account} options={{ orientation: 'all' }} />
                        <Stack.Screen name='About' component={About} options={{ orientation: 'all' }} />
                        <Stack.Screen name='Games' component={GameList} options={{ orientation: 'all' }} />
                        <Stack.Screen name='GameDetails' component={GameDetails} options={{ orientation: 'all' }} />
                        <Stack.Screen name='GameReservation' component={GameReservation} options={{ orientation: 'all' }} />
                    </>
                ) : (
                    <>
                        <Stack.Screen name='SignIn' component={SignIn} options={{ orientation: 'all' }} />
                        <Stack.Screen name='SignUp' component={SignUp} options={{ orientation: 'all' }} />
                    </>
                )}
            </Stack.Navigator>
        </GamesProvider>
    )
}

export default NavigationScreen