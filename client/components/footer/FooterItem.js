import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import themeContext from '../../context/theme';

const FooterItem = ({name, text, handlePress, screenName, routeName}) => {
    const theme = useContext(themeContext);
    const [state, setState] = useState();

    useEffect(() => {

        if(screenName === routeName){
            const color = 'orange';
            setState(color);
        } else {
            setState(theme.color);
        }
    })

    return (
        <TouchableOpacity onPress={handlePress}>
            <>
                <FontAwesome5 name={name} size={25} style={styles.fontStyle} color={state}/>
                <Text style={[styles.iconText, {color: theme.color}]}>{text}</Text>
            </>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    fontStyle: {
        marginBottom: 3,
        alignSelf: 'center',
    },
    iconText: {
        fontSize: 12,
        textAlign: 'center',
        textTransform: 'uppercase',
    },
})

export default FooterItem