import React, {useState, useContext} from 'react';
import {NativeModules, Text, TouchableOpacity, StyleSheet, View, Image} from 'react-native';
import ThemeContext from '../../context/theme';

const { UIManager } = NativeModules;



UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true);

const Licorne = () => {
    const theme = useContext(ThemeContext);

    state = {
        w: 100,
        h: 100,
    };

    const [width, setWidth] = useState(state.w);
    const [height, setHeight] = useState(state.h);


    SizeUpOnPress = () => {

        setWidth(width + 30);
        setHeight(height + 30);
    };

    SizeDownOnPress = () => {

        setWidth(width - 30);
        setHeight(height - 30);
    };

    return (
        <View style={styles.container}>
            <Image style={{ width: width, height: height }} source={require('../../assets/licorne.png')}/>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={SizeUpOnPress} style={{}}>
                    <View style={[styles.button, {backgroundColor: theme.licornButton}]}>
                        <Text style={[styles.buttonText, {color: theme.licornText}]}>Size up !</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={SizeDownOnPress}>
                    <View style={[styles.button, {backgroundColor: theme.licornButton}]}>
                        <Text style={[styles.buttonText, {color: theme.licornText}]}>Size down !</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        
        paddingHorizontal: 20,
        paddingVertical: 15,
        margin: 5,
    },
    buttonText: {
        fontWeight: 'bold',
    },
});

export default Licorne;