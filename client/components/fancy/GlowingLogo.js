import Animated, {useAnimatedStyle, withRepeat, withSequence, withTiming} from 'react-native-reanimated';
import {Image, View} from 'react-native';
import {containers} from '../style/Styles';

const GlowingLogo = () => {
    const glowAnimation = useAnimatedStyle(() => ({
        transform: [
            {
                scale: withRepeat(
                    withSequence(
                        withTiming(1.2, { duration: 1500 }),
                        withTiming(1.6, { duration: 1500 })
                    ),
                    -1,
                    true
                ),
            },
        ],
    }));

    return (
        <>
            <View style={{ alignItems: 'center', overflow: 'visible', marginTop: 5 }}>
                <Animated.View style={[{
                }, glowAnimation]}>
                    <Image source={require('../../assets/logo.png')} style={containers.logoStyle} />
                </Animated.View>
            </View>
        </>
    );
};

export default GlowingLogo;