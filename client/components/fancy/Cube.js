import React from 'react';
import {View} from 'react-native';
import Expo from 'expo';
import {Scene, Mesh, MeshBasicMaterial, MeshPhongMaterial, PerspectiveCamera, BoxGeometry, AmbientLight, DirectionalLight} from 'three';
import ExpoTHREE, {Renderer} from 'expo-three';
import { ExpoWebGlRenderingContext, GLView} from 'expo-gl';

const Cube = () => {

    const onContextCreate = async (gl) => {
        const scene = new Scene();
        const camera = new PerspectiveCamera(
            75,
            gl.drawingBufferWidth/gl.drawingBufferHeight,
            0.1,
            1000
        );

        gl.canvas = {width: gl.drawingBufferWidth, height: gl.drawingBufferHeight}
        camera.position.z = 2

        const renderer = new Renderer({gl})
        renderer.setSize(gl.drawingBufferWidth, gl.drawingBufferHeight)

        const geometry = new BoxGeometry(1, 1, 1)
        const material = new MeshPhongMaterial({
            color: 'orange',
        })
        const cube = new Mesh(geometry, material)
        scene.add(cube)
        scene.add(new AmbientLight(0x404040));

        const light = new DirectionalLight(0xffffff, 0.5);
        light.position.set(3, 3, 3);
        scene.add(light);


        const render = () => {
            requestAnimationFrame(render)
            cube.rotation.x += 0.01
            cube.rotation.y += 0.01
            renderer.render(scene, camera)
            gl.endFrameEXP()
        }

        render()


    }

    return (
        <View>
            <GLView
                onContextCreate={onContextCreate}
                style = {{width: 500, height: 500}}
            />
        </View>
    )
}

export default Cube