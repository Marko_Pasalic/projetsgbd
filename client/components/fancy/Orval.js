import React, {useState, useContext, useEffect} from 'react';
import {Text, TouchableOpacity, StyleSheet, View, Image} from 'react-native';
import ThemeContext from '../../context/theme';
import {Audio} from 'expo-av';


const Orval = () => {
    const theme = useContext(ThemeContext);
    const [sound, setSound] = useState();


    async function playSound() {
        const { sound } = await Audio.Sound.createAsync(require('../../assets/beerBottle.mp3')
        );
        setSound(sound);

        await sound.playAsync();
    }

    useEffect(() => {
        return sound
            ? () => {
                sound.unloadAsync();
            }
            : undefined;
    }, [sound]);

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={playSound}>
                <Image style={[styles.box, { width: 50, height: 150 }]} source={require('../../assets/orval.png')}/>
            </TouchableOpacity>
            <Text style={[styles.buttonText, {color: theme.color}]}>Click on me !</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {

        paddingHorizontal: 20,
        paddingVertical: 15,
        margin: 5,
    },
    buttonText: {
        fontWeight: 'bold',
    },
});

export default Orval;