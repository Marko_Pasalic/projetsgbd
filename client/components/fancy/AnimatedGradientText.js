import React, {useEffect, useState, useRef} from 'react';
import {Text} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import MaskedView from '@react-native-masked-view/masked-view';

const START_DEFAULT = { x: 1, y: 0 };
const END_DEFAULT = { x: 0, y: 0 };
const GRADIENT_COLORS = ["#ff0061", "#ffaa00", "#faf100", "#8ae800", "#00a7ea", "#7734ea", "#ff0061"];
const GRADIENT_LOCATIONS = [0, 0.15, 0.30, 0.45, 0.60, 0.75, 1];
const MOVEMENT = GRADIENT_LOCATIONS[1] / 20;
const INTERVAL = 20;

const AnimatedGradientText = (props) => {

    let [gradientOptions, setGradientOptions] = useState({
        colors: GRADIENT_COLORS,
        locations: GRADIENT_LOCATIONS,
        start: START_DEFAULT,
        end: END_DEFAULT
    });
    const gradientOptionsRef = useRef(gradientOptions);
    gradientOptionsRef.current = gradientOptions;

    let infiniteRainbow = () => {
        if (gradientOptionsRef.current.locations[1] - MOVEMENT <= 0) {

            let gradientColors = [...gradientOptionsRef.current.colors];
            gradientColors.shift();
            gradientColors.push(gradientColors[1]);

            setGradientOptions({
                colors: gradientColors,
                locations: GRADIENT_LOCATIONS,
                start: START_DEFAULT,
                end: END_DEFAULT
            });
        } else {
            let updatedLocations = gradientOptionsRef.current.locations.map((item, index) => {
                if (index === gradientOptionsRef.current.locations.length - 1) {
                    return 1;
                }

                return parseFloat(Math.max(0, item - MOVEMENT).toFixed(2));
            });

            setGradientOptions({
                colors: [...gradientOptionsRef.current.colors],
                locations: updatedLocations,
                start: START_DEFAULT,
                end: END_DEFAULT
            });
        }

        timeout = setTimeout(infiniteRainbow, INTERVAL);
    };

    useEffect(() => {
        infiniteRainbow();
    }, []);

    return (
        <MaskedView maskElement={<Text {...props} />}>
            <LinearGradient
                colors={gradientOptions.colors}
                locations={gradientOptions.locations}
                start={gradientOptions.start}
                end={gradientOptions.end}>
                <Text {...props} style={[props.style, { opacity: 0 }]} />
            </LinearGradient>
        </MaskedView>
    );
};

export default AnimatedGradientText;