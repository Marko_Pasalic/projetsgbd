import {StyleSheet} from 'react-native';

const TEXT_COLOR = '#ff5f00';
const MAIN_COLOR = 'orange';

const containers = StyleSheet.create({
    mainBox: {
        flex: 1,
        justifyContent: 'center',
    },
    spaceBetweenMainBox: {
        flex: 1,
        justifyContent: 'space-between',
    },
    logoContainer: { 
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoStyle: {
        width: 100,
        height: 100,
        marginVertical: 20,
    },
    box:{
        backgroundColor: '#fff',
        width: '92%',
        height: 300,
        borderRadius: 14,
        borderWidth: 0.8,
        shadowColor: TEXT_COLOR,
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.4,
        shadowRadius: 3,
        marginBottom: 20,
    },
    boxImage: {
        height: '70%',
        width: '100%',
        borderTopRightRadius: 14,
        borderTopLeftRadius: 14,
    },
    boxGameDetails:{
        backgroundColor: '#fff',
        alignSelf: 'auto',
        width: '92%',
        height: 'auto',
        marginBottom: 20,
    },
    boxImageGameDetails: {
        height: '100%',
        width: '50%',
        borderTopRightRadius: 14,
        borderTopLeftRadius: 14,
    },
    boxCommentGameDetails:{
        backgroundColor: '#fff',
        alignSelf: 'auto',
        width: '100%',
        height: 'auto',
        borderRadius: 10,
        borderWidth: 1.2,
        borderColor: '#3B3B3B',
        borderOffset: {width: -2, height: 4},
        shadowColor: TEXT_COLOR,
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        marginBottom: 5,
    },
    glowContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
})

const theme = StyleSheet.create({
    light: {
        theme: "light",
        color: "black",
        background: "white",
        licornButton: "black",
        licornText: "white",
    },
    dark: {
        theme: "dark",
        color: "white",
        background: "#35393e",
        licornButton: "white",
        licornText: "black",
    },
})

const texts = StyleSheet.create({
    formText: {
        fontSize: 16,
        color: TEXT_COLOR,
    },
    title: {
        fontSize: 30,
        textAlign: 'center',
    },
    mainText:{
        fontSize: 16,
        textAlign: 'justify',
        margin: 15,
    },
    accountText: {
        fontSize: 16,
        textAlign: 'center',
    },
})

const buttons = StyleSheet.create({
    buttonStyle: {
        backgroundColor: MAIN_COLOR,
        height: 50,
        marginBottom: 20,
        justifyContent: 'center',
        marginHorizontal: 15,
        borderRadius: 15,
    },
    disabledButtonStyle: {
        backgroundColor: 'grey',
        height: 50,
        marginBottom: 20,
        justifyContent: 'center',
        marginHorizontal: 15,
        borderRadius: 15,
    },
    buttonText: {
        fontSize: 20,
        textAlign: 'center',
        color: '#fff',
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
})

const forms = StyleSheet.create({
    input: {
        borderBottomWidth: 0.5,
        height: 48,
        borderBottomColor: TEXT_COLOR,
        marginBottom: 30,
    },
    commentInput: {
        borderColor: 'orange',
        borderWidth: 1,
        borderRadius: 15,
        padding: 5,
        margin: 10,
        height: 80,
    },
    inputLength: {
        textAlign: 'right',
        marginBottom: 5,
        marginRight: 5,
        color: 'grey',
    }
})

export {containers, theme, texts, buttons, forms, MAIN_COLOR, TEXT_COLOR}